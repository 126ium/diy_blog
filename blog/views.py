# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

import statsd

# Create your views here.
def index(request):
    client = statsd.StatsClient('localhost', 8125, prefix='blog')
    client.incr('access')
    return HttpResponse("Hello, world. You're at the blog's index.")
